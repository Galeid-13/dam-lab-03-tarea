import React, { Component } from 'react'
import {Text, View, Image} from 'react-native'
import AgeValidator from './app/components/AgeValidator'
import MyList from './app/components/MyList'
import Style from './app/components/Style'

const ageData = [
   {
      id: '1',
      age_min: 0,
      sub: 'Desde que naces a los 3 años',
      name: 'Bebe',
      image: require('./app/img/babyYoda.jpg'),
   },
   {
      id: '2',
      name: 'Niño',
      sub: 'Desde los 4 a 11 años',
      age_min: 4,
      image: require('./app/img/kid.jpg'),
   },
   {
      id: '3',
      name: 'Puberto',
      sub: 'Desde los 12 a 16 años',
      age_min: 12,
      image: require('./app/img/veAMisa.jpg'),
   },
   {
      id: '4',
      name: 'Joven',
      sub: 'Desde los 17 a 25 años',
      age_min: 17,
      image: require('./app/img/naruto.jpg'),
   },
   {
      id: '5',
      name: 'Joven Adulto',
      sub: 'Desde los 26 a 40 años',
      age_min: 26,
      image: require('./app/img/stonks.jpg'),
   },
   {
      id: '6',
      name: 'Boomer',
      sub: 'Desde los 41 a 59 años',
      age_min: 41,
      image: require('./app/img/vegetta777.jpg'),
   },
   {
      id: '7',
      name: 'Viejo',
      sub: 'Desde los 60 a 99 años',
      age_min: 60,
      image: require('./app/img/mister.jpg'),
   },
   {
      id: '8',
      name: 'F',
      sub: 'Ya deberías estar descansando...',
      age_min: 100,
      image: require('./app/img/ataudAfricanos.jpg'),
   },   
]

class App extends Component {
   constructor(props) {
      super(props)
      this.state = {
         data : ageData,
         youAre: '',
         imagen: require('./app/img/Dot.gif'),
      }
   }

   callbackFunction = (dataHijo) => {
      for (let i = ageData.length - 1; i > -1; i--) {
         if (dataHijo >= ageData[i].age_min) {
            this.setState({
               youAre: "Usted es un " + ageData[i].name,
               imagen: ageData[i].image,
            })
            break
         }
      }
   }

   render(){
      return(
         <View style={Style.mainContainer}>
            <Text style={Style.title}>
               Eres Mayor de Edad??? o No
            </Text>
            <View style={Style.subContainer}>
               <View style={Style.imageContainer} >
                  <View style={Style.imageBorder}>
                     <Image
                        style={Style.mainImage}
                        source={this.state.imagen}
                     />
                  </View>
               </View>
               <AgeValidator
                  callBackNroEdad={this.callbackFunction}
               />
            </View>
            <View style={Style.listContainer}>
               <Text style={Style.youAreText}>
                  {this.state.youAre}
               </Text>
               <MyList 
                  data={this.state.data}
               />
            </View>
         </View>
      )
   }
}

export default App
