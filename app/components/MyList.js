import React, {Component} from 'react'
import {View, Text, Image, TouchableOpacity, SafeAreaView, FlatList} from 'react-native'
import Style from './Style'

class MyList extends Component{
   constructor(props){
      super(props)
   }

   renderItem = ({item}) => (
      <TouchableOpacity>
         <View style={Style.itemContainer}>
            <View style={Style.itemImageContainer} >
               <Image
                  style={Style.itemImage}
                  source={item.image}
               />
            </View>
            <View>
               <Text style={Style.itemText}>
                     {item.name}
               </Text>
               <Text style={Style.itemSubText}>
                  {item.sub}
               </Text>
            </View>
         </View>
      </TouchableOpacity>
   )
   
   keyExtractor = (item, index) => index.toString()
   
   FlatListMarco = () => {
      return(
         <View style={Style.itemSeparator} />
      )
   }

   render(){
      return(
         <SafeAreaView style={Style.safeArea}>
            <FlatList
               style={Style.floatList}
               keyExtractor = {this.keyExtractor}
               data = {this.props.data}
               renderItem = {this.renderItem}
               ItemSeparatorComponent = {this.FlatListMarco}
            />
         </SafeAreaView>
      )
   }
}

export default MyList