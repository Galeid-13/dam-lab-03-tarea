import React, {Component} from 'react'
import {View, Text, TextInput} from 'react-native'
import Style from './Style'

class AgeValidator extends Component {
   constructor(props) {
      super(props)
      this.state = {
         textValue: '',
         age: 0,
         validationText: '¿Serás Legal o Ilegal?',
      }
   }

   enviarData = (edadN) => {
      this.props.callBackNroEdad(edadN)
   }

   changeTextInput = text => {
      console.log(text)
      let regexNum = /^[0-9]+$/
      let finalText = (text==='')? '': this.state.textValue
      let edadText = this.state.validationText

      if (regexNum.test(text)) {
         finalText = text

         edadText = (Number(finalText)>=18)? 'Eres Legal': 'Eres Ilegal'
         this.enviarData(Number(finalText))
      }
      
      this.setState({
         textValue: finalText,
         validationText: edadText,
         age: Number(finalText)
      })
   }

   render(){
      return(
         <View style={Style.ageValidatorContainer}>
            <TextInput
               autoFocus={true}
               style={Style.input}
               onChangeText={text => this.changeTextInput(text)}
               value={this.state.textValue}
               placeholder={'Ingrese un Número'}
               placeholderTextColor={'#8E9297'}
            />
            <Text style={Style.ageValidatorText}>
               {this.state.validationText}
            </Text>
         </View>
      )
   }
}

export default AgeValidator
