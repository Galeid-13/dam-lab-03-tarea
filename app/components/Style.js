import { StyleSheet } from "react-native"

const Style = StyleSheet.create({
   mainContainer: {
      flex: 1,
      backgroundColor: '#36393F',
   },
   title: {
      alignSelf: 'center',
      color: 'white',
      textAlign: 'center',
      paddingVertical: 15,
      fontWeight: 'bold',
      backgroundColor: '#7289DA',
      fontSize: 25,
      width: '96%',
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
   },
   subContainer: {
      flex: .4,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginHorizontal: 15,
   },
   imageContainer: {
      flex: .35,
      alignItems: 'center',
   },
   imageBorder: {
      backgroundColor: '#2F3136',
      marginVertical: 8,
      padding: 9,
      borderRadius: 10,
   },
   mainImage: {
      width: 80,
      height: 80,
      borderRadius: 10,
   },
   listContainer: {
      flex: 2,
   },
   youAreText: {
      textAlign: 'center',
      marginVertical: 8,
      paddingVertical: 5,
      fontSize: 20,
      fontFamily: 'Lobster-Regular',
      color: 'white',
      alignSelf: 'center',
      backgroundColor: '#2F3136',
      borderRadius: 38,
      width: 325,
   },
   ageValidatorContainer: {
      flex: .65,
      alignItems: 'center',
   },
   input: {
      marginTop: 15,
      paddingVertical: 7,
      textAlign: 'center',
      fontSize: 20,
      color: 'white',
      backgroundColor: '#202225',
      borderRadius: 38,
      width: '85%',
   },
   ageValidatorText: {
      fontSize: 20,
      fontFamily: 'Lobster-Regular',
      color: 'white',
      marginTop: 10,
   },
   itemSeparator: {
      height: 2,
      width: '88%',
      alignSelf: 'center',
      backgroundColor: '#202225',
   },
   safeArea: {
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 75,
   },
   floatList: {
      backgroundColor: '#2F3136',
      borderRadius: 25,
   },
   itemContainer: {
      flex: 1,
      flexDirection: 'row',
      marginLeft: 20,
      justifyContent: 'flex-start',
      margin: 15,
   },
   itemImageContainer: {
      marginRight: 20,
      width: 60,
      height: 60,
      borderRadius: 10,
      backgroundColor: '#8E9297',
   },
   itemImage: {
      marginRight: 20,
      width: 60,
      height: 60,
      borderRadius: 10,
   },
   itemText: {
      marginVertical: 4,
      fontSize: 20,
      fontFamily: 'Lobster-Regular',
      fontWeight: 'bold',
      color: 'white',
   },
   itemSubText: {
      fontSize: 14,
      color: '#8E9297',
   }
})

export default Style